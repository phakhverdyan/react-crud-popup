import React from "react";
import ReactDOM from "react-dom";
import CRUDTable, {
    Fields,
    Field,
    CreateForm,
    UpdateForm,
    DeleteForm
} from "react-crud-table";

// Component's Base CSS
import "./index.css";

const defaultItems = [
    {
        id: 1,
        email: "test1@test.com",
        name: "test1",
        phone: "111111111",
    },
    {
        id: 2,
        email: "test2@test.com",
        name: "test2",
        phone: "2222222222",
    }
];
let records = JSON.parse(localStorage.getItem("recordsData") || JSON.stringify(defaultItems));


const SORTERS = {
    NUMBER_ASCENDING: mapper => (a, b) => mapper(a) - mapper(b),
    NUMBER_DESCENDING: mapper => (a, b) => mapper(b) - mapper(a),
    STRING_ASCENDING: mapper => (a, b) => mapper(a).localeCompare(mapper(b)),
    STRING_DESCENDING: mapper => (a, b) => mapper(b).localeCompare(mapper(a))
};

const getSorter = data => {
    const mapper = x => x[data.field];
    let sorter = SORTERS.STRING_ASCENDING(mapper);

    if (data.field === "id") {
        sorter =
            data.direction === "ascending"
                ? SORTERS.NUMBER_ASCENDING(mapper)
                : SORTERS.NUMBER_DESCENDING(mapper);
    } else {
        sorter =
            data.direction === "ascending"
                ? SORTERS.STRING_ASCENDING(mapper)
                : SORTERS.STRING_DESCENDING(mapper);
    }

    return sorter;
};

let count = records.length;
const service = {
    fetchItems: payload => {
        let result = Array.from(records);
        result = result.sort(getSorter(payload.sort));
        return Promise.resolve(result);
    },
    create: record => {
        count += 1;
        records.push({
            ...record,
            id: count
        });
        localStorage.setItem('recordsData', JSON.stringify(records));

        return Promise.resolve(record);
    },
    update: data => {
        const record = records.find(t => t.id === data.id);
        record.name = data.name;
        record.email = data.email;
        record.phone = data.phone;
        localStorage.setItem('recordsData', JSON.stringify(records));

        return Promise.resolve(record);
    },
    delete: data => {
        const record = records.find(t => t.id === data.id);
        records = records.filter(t => t.id !== record.id);
        localStorage.setItem('recordsData', JSON.stringify(records));
        return Promise.resolve(record);
    },
    validateFields: values => {
        const errors = {};
        if (!values.name) {
            errors.name = "Please provide name";
        }

        if (!values.phone) {
            errors.phone = "Please provide phone number";
        }
        if(values.phone && !values.phone.match(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$/g))
        {
            errors.phone = "Please provide valid phone number";
        }

        if (!values.email) {
            errors.email = "Please, provide record's phone number";
        }
        return errors
    }
};

const styles = {
    container: {margin: "auto", width: "fit-content"}
};

const Example = () => (
    <div style={styles.container}>
        <CRUDTable
            caption="Records"
            fetchItems={payload => service.fetchItems(payload)}
        >
            <Fields>
                {/*<Field name="id" label="Id" hideInCreateForm/>*/}
                <Field name="name" label="Name" placeholder="Name"/>
                <Field name="email" label="Email" placeholder="Email"/>
                <Field name="phone" label="Phone" placeholder="Phone"/>
            </Fields>
            <CreateForm
                title="Record Creation"
                message="Add a new record!"
                trigger="Add Record"
                onSubmit={record => service.create(record)}
                submitText="Add"
                validate={values => service.validateFields(values)}
            />

            <UpdateForm
                title="Record Update Process"
                message="Update record"
                trigger="Edit"
                onSubmit={record => service.update(record)}
                submitText="Save"
                validate={values => service.validateFields(values)}
            />

            <DeleteForm
                title="Record Delete Process"
                message="Are you sure you want to delete the record?"
                trigger="Delete"
                onSubmit={record => service.delete(record)}
                submitText="Delete"
                validate={values => {
                    const errors = {};
                    if (!values.id) {
                        errors.id = "Please, provide id";
                    }
                    return errors;
                }}
            />
        </CRUDTable>
    </div>
);

Example.propTypes = {};

ReactDOM.render(<Example/>, document.getElementById("root"));
